### About

This sample **Spring Boot** project shows how to use **OpenFeign** client using `@EnableFeignClients` annotation. **OpenFeign** is a *declarative*  web service client. It makes writing web service clients easier. To use **OpenFeign**, we need to create an interface and annotate it.

---

### Code Example

`@EnableFeignClients` annotation is used on the application main class to make the application a **Feign Client**.


**Application Main class** (`com.example.fc.FeignClientExternalApplication`): This main class itself is a REST controller, as annotated with `@RestController` and provides a couple of REST API endpoints (e.g. `/posts/{postId}`). 

```java
@SpringBootApplication
@RestController
@EnableFeignClients
public class FeignClientExternalApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeignClientExternalApplication.class, args);
	}
	
	@Autowired
	JsonPlaceHolderClient jsonPlaceHolderClient;
	
	@RequestMapping("/posts")
	public List<Post> getPosts() {
		return jsonPlaceHolderClient.getPosts();
	}

	@RequestMapping("/posts/{postId}")
	public Post getPostById(@PathVariable("postId") Long postId) {
		return jsonPlaceHolderClient.getPostById(postId);
	}
}
```

---

**Feign Client Interface** (`com.example.fc.client.JsonPlaceHolderClient`): This interface is declared as a ``@FeignClient`` and provides the signature of two methods which consumes REST web services from [JSONPlaceholder](https://jsonplaceholder.typicode.com/). *JSONPlaceholder* is a *fake* online REST API for testing and prototyping.

```java
@FeignClient(value = "jplaceholder", url = "https://jsonplaceholder.typicode.com/")
public interface JsonPlaceHolderClient {
	@RequestMapping(method = RequestMethod.GET, value = "/posts")
	List<Post> getPosts();

	@RequestMapping(method = RequestMethod.GET, value = "/posts/{postId}", produces = "application/json")
	Post getPostById(@PathVariable("postId") Long postId);	
}
```

For example, if a **GET** request is sent to the ``https://jsonplaceholder.typicode.com/posts/1`` REST endpoint, a JSON document is received that looks like this:

```json
{
    "userId": 1,
    "id": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
}

```

And that's it!! By simply declaring intefaces, we are able to consume REST web services.

---

**Dockerfile** for creating Docker image of this application.

```
FROM openjdk:8-alpine
RUN mkdir -p /opt/sb-feign-client-external
WORKDIR /opt/sb-feign-client-external
COPY target/sb-feign-client-external-0.0.1-SNAPSHOT.jar /opt/sb-feign-client-external
EXPOSE 8080
CMD java -jar sb-feign-client-external-0.0.1-SNAPSHOT.jar
```

---

**Birbucket pipelines** for pushing the Docker image to Docker Registry [shameemsr/sb-feign-client-external](https://hub.docker.com/repository/docker/shameemsr/sb-feign-client-external).

```yaml
pipelines:
  default:
    # Build with Maven, Create & Push Docker Images to a Docker Registry
    - step:
        name: Build with Maven, Create & Push Docker Images to a Docker Registry
        script: 
          - mvn -B -DskipTests clean install
          - docker build -t $DOCKER_HUB_USERNAME/sb-feign-client-external:latest .
          - docker login --username $DOCKER_HUB_USERNAME --password $DOCKER_HUB_PASSWORD
          - docker push $DOCKER_HUB_USERNAME/sb-feign-client-external:latest
        services:
          - docker
        caches: 
          - docker
```

Docker Pull Command: `docker pull shameemsr/sb-feign-client-external`

---

**Maven** dependencies

```xml

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-openfeign</artifactId>
		</dependency>
```

---

### Reference Documentation
For further reference, please consider the following sections:


* [OpenFeign/feign](https://github.com/OpenFeign/feign)

* [Spring Cloud OpenFeign](https://cloud.spring.io/spring-cloud-openfeign/reference/html/)

* [JSONPlaceholder](https://jsonplaceholder.typicode.com/)

