FROM openjdk:8-alpine
RUN mkdir -p /opt/sb-feign-client-external
WORKDIR /opt/sb-feign-client-external
COPY target/sb-feign-client-external-0.0.1-SNAPSHOT.jar /opt/sb-feign-client-external
EXPOSE 8080
ENV JAVA_ARGS=""
#CMD ["java", "$JAVA_ARGS", "-jar", "sb-feign-client-external-0.0.1-SNAPSHOT.jar"]
CMD java $JAVA_ARGS -jar sb-feign-client-external-0.0.1-SNAPSHOT.jar