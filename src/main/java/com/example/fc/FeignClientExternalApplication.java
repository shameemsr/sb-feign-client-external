package com.example.fc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.fc.client.JsonPlaceHolderClient;
import com.example.fc.model.Post;

//@EnableDiscoveryClient
@SpringBootApplication
@RestController
@EnableFeignClients
public class FeignClientExternalApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeignClientExternalApplication.class, args);
	}
	
	@Autowired
	JsonPlaceHolderClient jsonPlaceHolderClient;
	
	@RequestMapping("/posts")
	public List<Post> getPosts() {
		return jsonPlaceHolderClient.getPosts();
	}

	@RequestMapping("/posts/{postId}")
	public Post getPostById(@PathVariable("postId") Long postId) {
		return jsonPlaceHolderClient.getPostById(postId);
	}

}
